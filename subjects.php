<!DOCTYPE html>
<html lang="uk">
<head>
    <link rel="stylesheet" href="assets/css/style.css">
    <meta charset="utf-8">
    <title>Пример страницы</title>
</head>
<body>
<header>
    <?php include ("menu.php");?>
</header>
<div class="tablediv">
    <fieldset>
        <legend>Навчальний план</legend>
            <table>
                <tr>
                    <td><b>Предмети:</b></td>
                    <td>АТП</td>
                    <td>ЧММ на ЕОМ</td>
                    <td>ЕТМТ</td>
                    <td>СПІМ</td>
                    <td>ТАК</td>
                    <td>Право</td>
                    <td>ОВП</td>
                </tr>
                <tr>
                    <td><b>Години:</b></td>
                    <td>50</td>
                    <td>50</td>
                    <td>50</td>
                    <td>50</td>
                    <td>50</td>
                    <td>50</td>
                    <td>50</td>
                </tr>
                <tr>
                    <td><b>Лекційні години:</b></td>
                    <td>50</td>
                    <td>50</td>
                    <td>50</td>
                    <td>50</td>
                    <td>50</td>
                    <td>50</td>
                    <td>50</td>
                </tr>
                <tr>
                    <td><b>Практичні години:</b></td>
                    <td>50</td>
                    <td>50</td>
                    <td>50</td>
                    <td>50</td>
                    <td>50</td>
                    <td>50</td>
                    <td>50</td>
                </tr>
                <tr>
                    <td><b>Лабораторні години:</b></td>
                    <td>50</td>
                    <td>50</td>
                    <td>50</td>
                    <td>50</td>
                    <td>50</td>
                    <td>50</td>
                    <td>50</td>
                </tr>
            </table>
    </fieldset>
</div>
</body>
</html>